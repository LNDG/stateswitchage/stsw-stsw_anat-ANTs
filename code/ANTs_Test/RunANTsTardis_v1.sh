#!/bin/bash

#REFDIR="/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/rest/preproc/B_data/C_standards"
#SOURCEDIR="/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/rest/preproc/B_data/D_preproc/2227/mri/t1"

REFDIR="/home/mpib/kosciessa/ANTs_Test"
OUTDIR="/home/mpib/kosciessa/ANTs_Test"
SOURCEDIR="/home/mpib/kosciessa/ANTs_Test"
inputImage="2227_t1_brain.nii.gz"
referenceImage="MNI152_T1_2mm_brain.nii.gz"

cd OUTDIR

antsRegistration --verbose 1 \
	--dimensionality 3 \
	--float 0 \
	--collapse-output-transforms 1 \
	–-output [brainFSintemp,brainFSintempWarped.nii.gz,brainFSintempInverseWarped.nii.gz] \
	--interpolation Linear \
	--use-histogram-matching 0 \
	--winsorize-image-intensities [0.005,0.995] \
	--initial-moving-transform [${REFDIR}/${referenceImage},${inputImage},1] \
	--transform Rigid[0.1] --metric MI[${REFDIR}/${referenceImage},${inputImage},1,32,Regular,0.25] \
	--convergence [1000x500x250x100,1e-6,10] \
	--shrink-factors 8x4x2x1 \
	--smoothing-sigmas 3x2x1x0vox \
	--transform Affine[0.1] \
	--metric MI[${REFDIR}/${referenceImage},${inputImage},1,32,Regular,0.25] \
	--convergence [1000x500x250x100,1e-6,10] \
	--shrink-factors 8x4x2x1 \
	--smoothing-sigmas 3x2x1x0vox \
	--transform SyN[0.1,3,0] \
	--metric CC[${REFDIR}/${referenceImage},${inputImage},1,4] \
	--convergence [100x70x50x20,1e-6,10] \
	--shrink-factors 8x4x2x1 \
	--smoothing-sigmas 3x2x1x0vox



