#!/bin/bash

DATAROOT="/Users/kosciessa/Desktop/mntTardisLNDG/LNDG/StateSwitch/WIP_ANTs/A_scripts/ANTs_Test"

c3d_affine_tool \
	-ref ${DATAROOT}/MNI152_T1_2mm_brain.nii.gz \
	-src ${DATAROOT}/2227_t1_f3R_brain.nii.gz \
	-itk ${DATAROOT}/MNIwoDura_affine_0GenericAffine.mat \
	-ras2fsl \
	-o ${DATAROOT}/MNIwoDura_affine_0GenericAffine_FSL.mat
	