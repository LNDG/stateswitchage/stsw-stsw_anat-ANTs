#!/bin/bash

SubjectID="1117 1118 1120 1124 1125 1126 1131 1132 1135 1136 1151 1160 1164 1167 1169 1172 1173 1178 1182 1214 1215 1216 1219 1223 1227 1228 1233 1234 1237 1239 1240 1243 1245 1247 1250 1252 1257 1261 1265 1266 1268 1270 1276 1281 2104 2107 2108 2112 2118 2120 2121 2123 2125 2129 2130 2131 2132 2133 2134 2135 2139 2140 2142 2145 2147 2149 2157 2160 2201 2202 2203 2205 2206 2209 2210 2211 2213 2214 2215 2216 2217 2219 2222 2224 2226 2227 2236 2237 2238 2241 2244 2246 2248 2250 2251 2252 2253 2254 2255 2258 2261"
#SubjectID="1117"

for SUB in ${SubjectID} ; do

	SCRIPTDIR="/home/mpib/LNDG/StateSwitch/WIP_ANTs/A_scripts"
	REFDIR="/home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/C_standards"
	OUTDIR="/home/mpib/LNDG/StateSwitch/WIP_ANTs/B_data/B_Outputs"
	SOURCEDIR="/home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/${SUB}/mri/t1"
	inputImage="${SUB}_t1_brain.nii.gz"
	referenceImage="MNI152_T1_2mm_brain.nii.gz"

	if [ -f ${OUTDIR}/${SUB}_brainMNIWarped.nii.gz ]; then
		continue
	fi
	
	cd ${SCRIPTDIR}/

	echo "#PBS -N ${SUB}_ANTs_v1" 										>> job # Job name 
	echo "#PBS -l walltime=1:00:00" 									>> job # Time until job is killed 
	echo "#PBS -l mem=20gb" 											>> job # Books 4gb RAM for the job 
	echo "#PBS -m n" 													>> job # Email notification on abort/end, use 'n' for no notification 
	echo "#PBS -o /home/mpib/LNDG/StateSwitch/WIP_ANTs/Y_log/" 			>> job # Write (output) log to group log folder 
	echo "#PBS -e /home/mpib/LNDG/StateSwitch/WIP_ANTs/Y_log/" 			>> job # Write (error) log to group log folder 
			
			
	echo "module load ants" >> job

	echo "antsRegistration \
		--verbose 1 \
		--dimensionality 3 \
		--float 0 \
		--output [${OUTDIR}/${SUB}_MNIwarps_,${OUTDIR}/${SUB}_brainMNIWarped.nii.gz] \
		--collapse-output-transforms 1 \
		--interpolation Linear \
		--use-histogram-matching 0 \
		--winsorize-image-intensities [0.005,0.995] \
		--initial-moving-transform [${REFDIR}/${referenceImage},${SOURCEDIR}/${inputImage},1] \
		--transform Rigid[0.1] \
		--metric MI[${REFDIR}/${referenceImage},${SOURCEDIR}/${inputImage},1,32,Regular,0.25] \
		--convergence [1000x500x250x100,1e-6,10] \
		--shrink-factors 8x4x2x1 \
		--smoothing-sigmas 3x2x1x0vox \
		--transform Affine[0.1] \
		--metric MI[${REFDIR}/${referenceImage},${SOURCEDIR}/${inputImage},1,32,Regular,0.25] \
		--convergence [1000x500x250x100,1e-6,10] \
		--shrink-factors 8x4x2x1 \
		--smoothing-sigmas 3x2x1x0vox \
		--transform SyN[0.1,3,0] \
		--metric CC[${REFDIR}/${referenceImage},${SOURCEDIR}/${inputImage},1,4] \
		--convergence [100x70x50x20,1e-6,10] \
		--shrink-factors 8x4x2x1 \
		--smoothing-sigmas 3x2x1x0vox" >> job
		
	qsub job
	rm job

done