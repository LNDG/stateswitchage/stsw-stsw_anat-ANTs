[![made-with-datalad](https://www.datalad.org/badges/made_with.svg)](https://datalad.org)

## Using ANTs for nonlinear registration of STSWD T1w to MNI

```
antsRegistration --verbose 1 --dimensionality 3 --float 0 --collapse-output-transforms 1 –output
[brainFSintemp,brainFSintempWarped.nii.gz,brainFSintempInverseWarped.nii.gz] --interpolation Linear --use-histogram-matching 0 --winsorize-image-intensities [0.005,0.995] --initial-moving-transform [${PARCDIR}/${parcimageref},brainFSbet.nii,1] --transform Rigid[0.1] --metric MI[${PARCDIR}/${parcimageref},brainFSbet.nii,1,32,Regular,0.25] --convergence [1000x500x250x100,1e-6,10] --shrink-factors 8x4x2x1 --smoothing-sigmas 3x2x1x0vox --transform Affine[0.1] --metric MI[${PARCDIR}/${parcimageref},brainFSbet.nii,1,32,Regular,0.25] --convergence [1000x500x250x100,1e-6,10] --shrink-factors 8x4x2x1 --smoothing-sigmas 3x2x1x0vox --transform SyN[0.1,3,0] --metric CC[${PARCDIR}/${parcimageref},brainFSbet.nii,1,4] --convergence [100x70x50x20,1e-6,10] --shrink-factors 8x4x2x1 --smoothing-sigmas 3x2x1x0vox
```

where, `${parcimageref}` is the reference, and `brainFSbet.nii` the input


you will also need the below c3d_affine_tool to convert the transform to something useable in fsl etc.

```
c3d_affine_tool -ref brainFSintempWarped.nii.gz -src brainFSbet.nii -itk brainFSintemp0GenericAffine.mat -ras2fsl -o brainFSintemp-fsl.mat
```

for c3d, see http://www.itksnap.org/pmwiki/pmwiki.php?n=Convert3D.Documentation

Recommendation from Petra Ritter:

1. preprocess fMRI
2. 6DOF BOLD -> T1w
3. epi_reg fMRI -> T1w
4. 6 DOF T1w -> MNI
5. Nonlinear T1w -> MNI (ANTs?)
6. concatenate all transformations / warps to get fMRI to MNI

Steps four and five would be done using ANTs (the 6DOF would likely be similar to the initial moving transform). Alternatively, we could also apply the 6 DOF first and then feed these data into ANTs.

We probably first have to get better brain extractions. The current ones include much CSF/dura.
