General Registration Discussion Thread

From: Randy McIntosh <rmcintosh@research.baycrest.org>

Hi all - still working thru this but I remain concerned that several of the FC matrices that we are using as targets for simulation may be artifactual b/c of the definition of ROIs.

I have plotted a couple of timeseries data from AD data and there are indeed large spikes in the data that will of course bias the FC estimates.  The problem is that, aside from visual examination, it is not trivial to know whether such excursions in the timeseries are real or not.

@Joelle - any word from Allistar?

@Petra - are there some fMRI timeseries that we can check out to see if they are more stable?




On Thu, Nov 9, 2017 at 1:40 PM, Kelly Shen <kshen@research.baycrest.org> wrote:
Thanks Petra. That’s really helpful. For registration, we use BBR but only do everything you describe up to step 4 before concatenation.  so it would probably help to add the additional step 5 of registering BOLD to EPI-MNI.

We will have to check what kind of field map data is available in the databases. I recall seeing some documentation that it’s part of the protocols but not sure if they are phase reversed encoding.


On Nov 9, 2017, at 1:24 PM, Ritter, Petra <petra.ritter@charite.de> wrote:
Hi Randy, hi All,
 
If fieldmaps are not available to correct distortions (which I suspect), a very good registration algorithm (boundary based registration) is used to warp fMRI to T1w which is capable of ameliorating distortions (even in the absence of a fieldmap/phase-rev): perform a nonlinear registration fMRI -> T1w via boundary based registration BBR fMRI -> T1 (tool epi_reg)
https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/FLIRT/UserGuide#epi_reg
Maybe the strategy is not 100 % perfect because the warpfield W1 might produce errors since it warps the distortions at the wrong places. Hence:
1. preprocess fMRI
2. epi_reg fMRI -> T1w (maybe 6DOF fMRI -> T1w first)
3. 6 DOF T1w -> MNI, Nonlinear T1w -> MNI
4. concatenate all transformations / warps to get fMRI to MNI
5. If alignment is not good: additional warp fMRI to EPI-MNI
6. After all registrations / warps have been done --> concatenate and do one-step resampling of initial fMRI to MNI
 
Regarding Kelly’s question: If phase reverse images ARE available (we are recording them now as a standard), the problem is addressed by phase reverse images / fieldmap correction. See Glasser et al. 2013: https://www.ncbi.nlm.nih.gov/pubmed/23668970 
"EPI fMRI images contain significant distortion in the phase encoding direction. This distortion can be corrected with either a regular (gradient-echo) fieldmap or a pair of spin echo EPI scans with opposite phase encoding directions. Reversing the phase encoding direction reverses the direction of the B0 field inhomogeneity effect on the images. The FSL toolbox “topup” (Andersson et al., 2003) can then be used to estimate the distortion field. The distorted spin echo EPI images are aligned with a 6 DOF FLIRT registration to the distorted gradient echo EPI single-band reference image. This registration is concatenated with the topup-derived distortion field, producing a nonlinear transform that can be used to undistort the fMRI images. The single-band reference image is corrected for distortions using spline interpolation and is then registered to the T1w image, first using 6 DOF FLIRT with the BBR cost function and using FreeSurfer’s BBRegister for fine tuning (Greve and Fischl, 2009). The result is an accurate registration between the fMRI data and the structural data, which is important for volume to surface mapping in the fMRISurface pipeline."
The pipelines can be found here: https://github.com/Washington-University/Pipelines
Another possibility would be post-hoc acquisition of a group average field maps – but I assume this is not an option given imaging was not performed at Baycrest.
 
I will check with Paul, who has processed our local AD imaging data (Charite acquired) whether he run in any problems.
 
Best,
Petra
 
From: Kelly Shen <kshen@research.baycrest.org>
Date: Thursday, 9. November 2017 at 16:40
To: Randy McIntosh <rmcintosh@research.baycrest.org>
Cc: Joelle Zimmermann <jzimmermann@research.baycrest.org>, "solodkin@uci.edu" <solodkin@uci.edu>, Petra Ritter <petra.ritter@charite.de>, Tanya Brown <tbrown@research.baycrest.org>, Zheng Wang <zwang@research.baycrest.org>
Subject: Re: ROI data extraction from BOLD images in our data processing pipeline - IMPORTANT
 
One other thing to consider is doing field map correction for these data. We don’t currently do it but field maps are available. I think the only issue is that doing field map correction in FSL is easy for data acquired on Siemens scanners but not that straightforward for data acquired on other scanners (and it’s a mix of scanners in ADNI and PPMI data). See: https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/FUGUE/Guide
 
If anyone in the group has some experience doing this, your input would be appreciated.
 
Thanks,
Kelly

```
On Nov 9, 2017, at 9:47 AM, Randy McIntosh <rmcintosh@research.baycrest.org> wrote:

Thanks Kelly.  I do think we need a way to flag ROIs that are dodgy, especially since we are sending these to Viktor's group to model.  Lets see what Joell, Ana and Petra say and then can decide how best to move on this
 
On Thu, Nov 9, 2017 at 9:15 AM, Kelly Shen <kshen@research.baycrest.org> wrote:
We have an extra bridge step in registration (step 3) for older adult data where we register MNI to a 70 year old average template brain before registration to subject T1. This helps with normal age-related atrophy but doesn’t do well for extreme atrophy in disease. We’ve considered creating a study-specific brain template (eg: dementia average brain) but wasn’t sure it was worth the time to do this especially for such a large sample size.
 
In our implementation we don’t otherwise do anything for misaligned ROIs. We could consider computing BOLD SNR for each ROI and picking some sort of cutoff for discarding either individual ROIs or subjects entirely? Or computing SNR for each voxel and counting proportion of low SNR voxels per ROI and discarding ROIs with lots of bad voxels?
 
Kelly
```

On Nov 9, 2017, at 7:37 AM, Randy McIntosh <rmcintosh@research.baycrest.org> wrote:
Hi all - I've expanded this email address list so that we can get some consensus and clarity on an issue that I have noted and have yet to get a good answer.
 
In our standard MRI processing pipeline the steps are roughly as follows:
1) identify brain in T1 image
2) tissue segmentation on T1
3) warp T1 to the MNI template and save transform matrix
 (NB ROI template that we use is defined in MNI space)
4) motion correct BOLD data
5) co-register T1 and BOLD in native space
6) mask white matter and CSF from co-registered BOLD data 
7) extract ROI values from BOLD using the grey-matter defined areas by moving the ROI template to native space 
8) next subject
 
I have been helping out on this and have notes in step 5 (T1 to BOLD) that there can be severe misalignment because of the susceptibility artifact in EPI BOLD data. This is made worse when there is atrophy, like aging and dementia.  Hence, step 7 can be problematic for ROIs where there is either no BOLD data, or the T1 and BOLD are misaligned.
 
So, in the case there an ROI defined from a T1 segmented image does not align with the BOLD data, how does our pipeline deal with it? Is the ROI set to NaN or flagged as having low certainty? I have seen data (mainly FC matrices) where there are entire rows that look odd and these are often ROIs that are in the danger zone for this problem, like temporal poles and orbitofrontal.
 
The problem, of course, is that we are using these data for fitting TVB models.  If the data are suspect because of the issue, then the TVB models are similarly suspect.
 
please share your thoughts.
 
Randy